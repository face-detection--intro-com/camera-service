package com.introcom.cameraservice.controller;

import com.introcom.cameraservice.model.Camera;
import com.introcom.cameraservice.service.CameraService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})

public class CameraController {

    private CameraService cameraService;

    @Autowired
    public CameraController(CameraService cameraService) {
        this.cameraService = cameraService;
    }

    @PostMapping("")
    @ApiOperation(value = "Add a camera")
    public ResponseEntity<Camera> addCamera(@RequestBody Camera camera){
        return cameraService.addCamera(camera)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
    @PutMapping("")
    @ApiOperation(value = "Update a camera")
    public ResponseEntity<Camera> updateCamera(@RequestBody Camera camera){
        return cameraService
                .updateCamera(camera)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
    @DeleteMapping("")
    @ApiOperation(value = "Delete a camera")
    public ResponseEntity<Boolean> deleteCamera( Long cameraId){
        return cameraService.deleteCamera(cameraId)?
                ResponseEntity.ok().build()
                :
                ResponseEntity.badRequest().build();
    }
    @GetMapping("/{id}")
    @ApiOperation(value = "Get a camera by id")
    public ResponseEntity<Camera> getCamera(@PathVariable Long id){
        return cameraService
                .getCamera(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    @GetMapping("")
    @ApiOperation(value = "View a list of cameras")
    public ResponseEntity<List<Camera>> getCameras(){
        return ResponseEntity.ok(cameraService.getAllCameras());

    }
}
