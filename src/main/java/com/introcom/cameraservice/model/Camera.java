package com.introcom.cameraservice.model;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


@Document
public class Camera {
    @Id
    @ApiModelProperty(notes = "A randomly generated UUID for the database")
    private String id;
    @ApiModelProperty(notes = "The camera ID (this is sent by the camera and should be unique to that camera)")
    private Long cameraId;
    @ApiModelProperty(notes = "A name to show on menus instead of the ID")
    private String name;
    @ApiModelProperty(notes = "The brand of the camera")
    private String brand;
    @ApiModelProperty(notes = "The model of the camera")
    private String model;
    @ApiModelProperty(notes = "The status of the camera, if this is inactive, reports from the camera will be ignored")
    private Boolean active;

    public Camera() {
        this.active = true;
    }


    public String getId() {
        return id;
    }

    public Long getCameraId() {
        return cameraId;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void desactive() {
        this.active = false;
    }

    public void active() {
        this.active = true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCameraId(Long cameraId) {
        this.cameraId = cameraId;
    }
}
