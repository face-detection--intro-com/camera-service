package com.introcom.cameraservice.repository;

import com.introcom.cameraservice.model.Camera;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CameraRepository extends CrudRepository<Camera, String> {
    Optional<Camera> findByCameraId (Long id);
    Boolean existsByCameraIdAndActiveIsTrue (Long id);
    Boolean existsByCameraIdAndActiveIsFalse(Long id);
    @Override
    List<Camera> findAll();
}
