package com.introcom.cameraservice.service;

import com.introcom.cameraservice.model.Camera;
import com.introcom.cameraservice.repository.CameraRepository;
import com.introcom.cameraservice.util.CameraServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CameraService {

    private CameraRepository cameraRepository;

    @Autowired
    public CameraService(CameraRepository cameraRepository) {
        this.cameraRepository = cameraRepository;
    }

    public Optional<Camera> addCamera(Camera camera){
        camera.setActive(true);
        camera.setId( UUID.randomUUID().toString());
        if (cameraRepository.existsByCameraIdAndActiveIsTrue(camera.getCameraId()))
            return Optional.empty();
        return Optional.of(cameraRepository.save(camera));
    }
    public Boolean deleteCamera(Long cameraId){
        Optional<Camera> cameraModel = cameraRepository.findByCameraId(cameraId);
        if (!cameraModel.isPresent())
            throw new CameraServiceException("The camera doesn't exist");
        cameraModel.ifPresent(Camera::desactive);
        return true;
    }
    public Optional<Camera> updateCamera(Camera camera){
        if (!cameraRepository.existsById(camera.getId()))
            throw new CameraServiceException("The camera doesn't exist");
        return Optional.of(cameraRepository.save(camera));
    }
    public Optional<Camera> getCamera(Long cameraId){
        return cameraRepository.findByCameraId(cameraId);
    }

    public List<Camera> getAllCameras() {
        return cameraRepository.findAll();
    }
}
