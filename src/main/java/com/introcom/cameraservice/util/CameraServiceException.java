package com.introcom.cameraservice.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CameraServiceException extends RuntimeException {

    public CameraServiceException(String s) {
        super(s);
    }
}
